ObsTAPQuery
===========

A simple TAP (Table Access Protocol) Query generator

![obstapquery.png](https://bitbucket.org/repo/ynzddG/images/2115865309-obstapquery.png)

Getting started :

1. Download ObsTapQuery.jar from GitHub (Right click > Save as...)
2. Launch Aladin (http://aladin.u-strasbg.fr/aladin.gml)
3. Go to Tool > Plugins > Plugin controller...
4. Click on 'Local add...'
5. Select the ObsTAPQuery.jar file
6. Click on 'ObsTAP Query' in the left area
7. Click 'Start' button
 